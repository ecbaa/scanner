package com.sandeep.demo;

import java.util.Scanner;

public class Vowel {


	static void vowel() {
		System.out.println("Enter any one character");
		Scanner scan = new Scanner(System.in);
		String ch = scan.next();
		switch (ch) {
		case "a":
			System.out.println("Vowel");
			break;
		case "e":
			System.out.println("Vowel");
			break;
		case "i":
			System.out.println("Vowel");
			break;
		case "o":
			System.out.println("Vowel");
			break;
		case "u":
			System.out.println("Vowel");
			break;
		case "A":
			System.out.println("Vowel");
			break;
		case "E":
			System.out.println("Vowel");
			break;
		case "I":
			System.out.println("Vowel");
			break;
		case "O":
			System.out.println("Vowel");
			break;
		case "U":
			System.out.println("Vowel");
			break;
		default:
			System.out.println("Consonant");
			
			scan.close();
		}
	}

}
